from PySide2 import QtWidgets
from PySide2.QtUiTools import QUiLoader
import sys
import os
import os.path
from btchip.btchip import *
from btchip.btchipUtils import *
import binascii
import base64
import hashlib

class AppWindow():
    def __init__(self):
        super().__init__()
        loader = QUiLoader()
        bundle_dir = getattr(sys, '_MEIPASS', os.path.abspath(os.path.dirname(__file__)))
        path_to_dialog_ui = os.path.abspath(os.path.join(bundle_dir, 'dialog.ui'))
        self.ui = loader.load(path_to_dialog_ui, None)

        self.ui.btn_sign.clicked.connect(self.on_btn_sign_clicked_send)

    def on_btn_sign_clicked_send(self):
        derivation = self.ui.txt_derivation.text()
        message = self.ui.txt_message.toPlainText()
        print('message:')
        print(message)
        try:
            dongle = getDongle(True)
            app = btchip(dongle)
            print('btchip firmware version:')
            print(app.getFirmwareVersion())

            address = str(app.getWalletPublicKey(derivation)['address']).split("'")[1]
            print('address:')
            print(address)
            self.ui.txt_address.setText(address)

            hash = hashlib.sha256(message.encode("utf-8")).hexdigest()
            print('hash:')
            print(hash)
            self.ui.txt_hash.setText(hash)
            self.ui.txt_signature.setText("...")

            app.signMessagePrepare(derivation, message.encode("utf-8"))
            signature = binascii.hexlify(app.signMessageSign(""))
            signature = base64.b64encode(bytearray.fromhex(signature.hex())).decode()
            print('signature:')
            print(signature)
            self.ui.txt_signature.setText(signature)

            self.ui.repaint()
        except Exception as e:
            print(e)
            self.ui.txt_signature.setText('Connect Ledger and open Bitcoin app!')

            self.ui.repaint()

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    w = AppWindow()
    w.ui.show()
    sys.exit(app.exec_())

#!/bin/sh

# create executable
. venv3/bin/activate
pyinstaller --name="LedgerGUI" --icon="icons/icon.icns" --hidden-import PySide2.QtXml --add-data "dialog.ui:." --windowed main.py
deactivate

# cleanup
rm -rf build
rm -rf LedgerGUI.spec

# create dmg image
rm -rf dist/LedgerGUI
hdiutil create -volname LedgerGUI -srcfolder dist -ov -format UDZO LedgerGUI.dmg
rm -rf dist


pyinstaller.exe --name="LedgerGUI" --icon="icons/icon.ico" --hidden-import PySide2.QtXml --add-data "dialog.ui;." --onefile --windowed main.py

del build
cd dist
del LedgerGUI
cd ..

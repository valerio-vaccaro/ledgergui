# LedgerGui
Use Ledger via a python Qt5 GUI

## Install
Create virtual enviroment and install needed libraries.

```
virtualenv -p python3.8 venv3
source venv3/bin/activate
pip install -r requirements.txt
deactivate
```

## Usage
You can easily start the app running the file `main.py`

```
source venv3/bin/activate
python main.py
cd -
deactivate
```

## Modify the GUI
Using the designer for QT5 you can modify the dialog.ui that contain all the graphical structure of the app.

## Build a package
In order to build an executable package and an installer you can use the `build_*` scripts.

## Binaries
Binaries are available at [vaccaro.tech/ledgergui](http://vaccaro.tech/ledgergui/).
